import { Component, OnInit, Input } from '@angular/core';
import { IPost } from 'src/app/domain/IPost';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {
variable:string; 

  @Input() postTitle: string;
  @Input() postContent: string;
  @Input() date: Date;
  loveIt: number = 0;
  constructor() { }

  ngOnInit() {
    
  }
  success(): string{
    this.loveIt=this.loveIt+1;
    if (this.loveIt>0){
    return this.variable =  "list-group-item-success";
    }
    else if (this.loveIt<0){
      return this.variable =  "list-group-item-danger";
    }
    else return "";

  }
  danger(): string {
    this.loveIt=this.loveIt-1;
    if (this.loveIt>0){
      return this.variable =  "list-group-item-success";
      }
      else if (this.loveIt<0){
        return this.variable =  "list-group-item-danger";
      }
      else return this.variable= "";
  }
}
