import { Component, Input } from '@angular/core';
import { IPost } from './domain/IPost';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  number1: number;
  _date: Date = new Date();
  number2: number;
  data: IPost[] = [
    {id: '1',
      title: 'Mon premier post',
      content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis, nulla. Sequi ex totam dicta? Quia autem reprehenderit nostrum. Omnis reprehenderit velit eaque odio dignissimos neque.',
      loveIts: this.number1,
      created_at: this._date},

      {id: '2',
      title: 'Mon deuxième post',
      content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis, nulla. Sequi ex totam dicta? Quia autem reprehenderit nostrum. Omnis reprehenderit velit eaque odio dignissimos neque.',
      loveIts: this.number2,
      created_at: this._date},

      {id: '3',
      title: 'Encore un post',
      content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis, nulla. Sequi ex totam dicta? Quia autem reprehenderit nostrum. Omnis reprehenderit velit eaque odio dignissimos neque.',
      loveIts: this.number2,
      created_at: this._date}
   
  ] ;
  title = 'Posts';
}
